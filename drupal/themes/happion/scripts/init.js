jQuery(document).ready(function(){

	//remove Home button from header menu
	jQuery('#main-menu a:contains(Home)').parent().remove();

	//Add blocks toggle button
	jQuery('.sidebar .block').prepend('<i class="fa fa-caret-square-o-down block-toggle"></i>');
	jQuery('.sidebar .block i.fa').click(function(){
		jQuery(this).parent().find('.content').slideToggle('300');
		jQuery(this).toggleClass('act');
	})	
})